package com.example.muzmatchtest.features.utils

import com.example.muzmatchtest.features.mainactivity.model.ChatMessage
import com.example.muzmatchtest.features.mainactivity.model.MessageTypeOfView

class MessageAdapterUtils {

    companion object {

        fun getKindOfView(chatMessage: ChatMessage, userWhoSentMessage: String) : Int {
            return if(userWhoSentMessage == chatMessage.userSender) {
                MessageTypeOfView.VIEW_MESSAGE_SEND_BY_ME.ordinal
            } else {
                MessageTypeOfView.VIEW_MESSAGE_SEND_BY_OTHER.ordinal
            }
        }
    }

}