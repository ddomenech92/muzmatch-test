package com.example.muzmatchtest.features.mainactivity.model;

public enum MessageTypeOfView {
    VIEW_MESSAGE_SEND_BY_ME,
    VIEW_MESSAGE_SEND_BY_OTHER
}
