package com.example.muzmatchtest.features.mainactivity.view.adapter.viewholders

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.muzmatchtest.R
import com.example.muzmatchtest.features.mainactivity.model.ChatMessage
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MessagesViewHolder(itemView: View, private val isSentByMe: Boolean) : RecyclerView.ViewHolder(itemView) {

    private lateinit var chatMessage : ChatMessage
    private var chatPreviousMessage : ChatMessage? = null
    private var chatNextMessage : ChatMessage? = null

    private lateinit var messageContainer : ConstraintLayout
    private lateinit var messageText : TextView
    private lateinit var timeContainer : ConstraintLayout
    private lateinit var dayTime : TextView
    private lateinit var houtTime : TextView

    init {
        bindViews()
    }

    private fun bindViews() {
        messageContainer = itemView.findViewById(R.id.message_view_holder_message_container)
        messageText = itemView.findViewById(R.id.message_view_holder_message_text)
        timeContainer = itemView.findViewById(R.id.message_view_holder_time_container)
        dayTime = itemView.findViewById(R.id.view_message_tiem_section_day_text)
        houtTime = itemView.findViewById(R.id.view_message_tiem_section_hour_text)
    }

    fun bindData(chatMessage: ChatMessage) {
        this.chatMessage = chatMessage
        messageText.text = chatMessage.text
        checkPreviousMessage()
        checkNextMessage()
    }

    fun setPreviousMessage(chatPreviousMessage: ChatMessage?) {
        this.chatPreviousMessage = chatPreviousMessage
    }

    fun setNextMessage(chatNextMessage : ChatMessage?) {
        this.chatNextMessage = chatNextMessage
    }

    private fun checkPreviousMessage() {
        if(isMostOldMessageFromChat() || previousMessageIsMoreThan1HourAgo()) {
            showMessageTime()
        }
        else {
            hideMessageTime()
        }
    }

    private fun isMostOldMessageFromChat() : Boolean {
        return chatPreviousMessage == null
    }

    private fun previousMessageIsMoreThan1HourAgo() : Boolean {
        val diffInMs = chatMessage.date.time - chatPreviousMessage!!.date.time
        val diffInMin = TimeUnit.MILLISECONDS.toMinutes(diffInMs)
        return diffInMin > 60
    }

    private fun showMessageTime() {
        timeContainer.visibility = View.VISIBLE

        var simpleDateFormat = SimpleDateFormat("EEEE", Locale.UK)
        val dayOfWeek = simpleDateFormat.format(chatMessage.date)

        simpleDateFormat = SimpleDateFormat("hh:mm", Locale.FRANCE)
        val hour = simpleDateFormat.format(chatMessage.date)

        dayTime.text = dayOfWeek
        houtTime.text = hour
    }

    private fun hideMessageTime() {
        timeContainer.visibility = View.GONE
    }

    private fun checkNextMessage() {
        if(isMostRecentMessageFromChat()) {
            setMessageTail()
        }
        else {
            if(nextMessageIsFromOtherUser() || nextMessageIs20secondsAfter()) {
                setMessageTail()
            }
            else {
                removeMessageTail()
            }
        }
    }

    private fun isMostRecentMessageFromChat() : Boolean {
        return chatNextMessage == null
    }

    private fun nextMessageIsFromOtherUser() : Boolean {
        return chatMessage.userSender != chatNextMessage?.userSender
    }

    private fun nextMessageIs20secondsAfter() : Boolean {
        val diffInMs = chatNextMessage!!.date.time - chatMessage.date.time
        val diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs)
        return diffInSec > 20
    }

    private fun setMessageTail() {
        if(isSentByMe) {
            messageContainer.setBackgroundResource(R.drawable.view_bubble_message_send_by_me_with_tail)
        }
        else {
            messageContainer.setBackgroundResource(R.drawable.view_bubble_message_send_by_other_with_tail)
        }
    }

    private fun removeMessageTail() {
        if(isSentByMe) {
            messageContainer.setBackgroundResource(R.drawable.view_bubble_message_send_by_me)
        }
        else {
            messageContainer.setBackgroundResource(R.drawable.view_bubble_message_send_by_other)
        }
    }

}