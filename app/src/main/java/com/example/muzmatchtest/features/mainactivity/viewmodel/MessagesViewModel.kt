package com.example.muzmatchtest.features.mainactivity.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.muzmatchtest.features.mainactivity.model.ChatMessage
import com.example.muzmatchtest.realm.MessagesDao
import io.realm.Realm
import io.realm.RealmResults

class MessagesViewModel : ViewModel() {

    private val realmDB : Realm by lazy {
        Realm.getDefaultInstance()
    }

    private fun Realm.messagesDao() : MessagesDao = MessagesDao(this)

    fun getMessages() : LiveData<RealmResults<ChatMessage>> {
        return realmDB.messagesDao().getMessages()
    }

    fun addMessage(chatMessage: ChatMessage) {
        realmDB.messagesDao().addMessage(chatMessage)
    }

    fun clearMessages() {
        realmDB.messagesDao().clearMessages()
    }

    override fun onCleared() {
        realmDB.close()
        super.onCleared()
    }

}