package com.example.muzmatchtest.features.mainactivity.view.activity

import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.muzmatchtest.MyApplication
import com.example.muzmatchtest.R
import com.example.muzmatchtest.features.mainactivity.model.ChatMessage
import com.example.muzmatchtest.features.mainactivity.view.adapter.MessagesAdapter
import com.example.muzmatchtest.features.mainactivity.viewmodel.MessagesViewModel
import io.realm.RealmResults
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var messagesViewModel : MessagesViewModel

    private lateinit var userNameTextView : TextView
    private lateinit var messagesRecyclerView: RecyclerView
    private lateinit var sendMessageEditText : EditText
    private lateinit var sendMessageButton : Button

    private lateinit var adapter: MessagesAdapter

    private val chrisUser = "Chris"
    private val sarahUser = "Sarah"

    private var loggedUser : String = chrisUser
    private var otherUser : String = sarahUser

    private lateinit var messagesList : ArrayList<ChatMessage>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayShowTitleEnabled(false)

        initDagger()
        bindViews()
        setOtherUserNameInToolbar()
        setUpAdapter()
        setListeners()
        loadConversationMessages()
    }

    private fun initDagger() {
        (applicationContext as MyApplication).muzmatchComponent.inject(this)
    }

    private fun bindViews() {
        userNameTextView = findViewById(R.id.activity_main_toolbar_user_name_text_view)
        messagesRecyclerView = findViewById(R.id.activity_main_messages_recycler_view)
        sendMessageEditText = findViewById(R.id.activity_main_send_message_edit_text)
        sendMessageButton = findViewById(R.id.activity_main_send_message_button)
    }

    private fun setOtherUserNameInToolbar() {
        userNameTextView.text = otherUser
    }

    private fun loadConversationMessages() {
        messagesViewModel.getMessages().observe(this, {
            messagesLoaded -> onMessagesLoaded(messagesLoaded)
        })
    }

    private fun onMessagesLoaded(realmResults: RealmResults<ChatMessage>) {
        realmResults.let {
            messagesList.clear()
            for(message in it) {
                messagesList.add(message)
            }

            if(messagesList.size == 0) {
                //Messages just cleared
                adapter.notifyDataSetChanged()
            } else {
                //New message created
                adapter.notifyItemInserted(messagesList.size-1)
                if(messagesList.size > 1) {
                    //Update previous message to manage bubble message tail
                    adapter.notifyItemChanged(messagesList.size-2)
                }
            }
            scrollRecyclerToBottom()
        }
    }

    private fun setUpAdapter() {
        messagesList = ArrayList()
        adapter = MessagesAdapter(messagesList, loggedUser)
        messagesRecyclerView.layoutManager = LinearLayoutManager(this)
        messagesRecyclerView.adapter = adapter
    }

    private fun setListeners() {
        sendMessageButton.setOnClickListener {
            if(!TextUtils.isEmpty(sendMessageEditText.text)) {
                createMessageAndClearEditText()
            }
        }
        sendMessageEditText.setOnClickListener {
            scrollRecyclerToBottom()
        }
    }

    private fun createMessageAndClearEditText() {
        val newChatMessage = ChatMessage()
        newChatMessage.userSender = loggedUser
        newChatMessage.userReceiver = otherUser
        newChatMessage.text = sendMessageEditText.text.toString()
        newChatMessage.date = Date()

        messagesViewModel.addMessage(newChatMessage)

        sendMessageEditText.text.clear()
    }

    private fun scrollRecyclerToBottom() {
        messagesRecyclerView.scrollToPosition(messagesList.size-1)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_switch_users -> {
                switchUsers()
                true
            }
            R.id.action_clear_chat -> {
                clearChat()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun switchUsers() {
        if(loggedUser == chrisUser) {
            loggedUser = sarahUser
            otherUser = chrisUser
        }
        else {
            loggedUser = chrisUser
            otherUser = sarahUser
        }
        adapter.setLoggedUser(loggedUser)
        adapter.notifyDataSetChanged()
        setOtherUserNameInToolbar()
    }

    private fun clearChat() {
        messagesViewModel.clearMessages()
    }
}