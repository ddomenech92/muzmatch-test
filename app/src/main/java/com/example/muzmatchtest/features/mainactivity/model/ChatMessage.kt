package com.example.muzmatchtest.features.mainactivity.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import java.util.*

open class ChatMessage(@SerializedName(value = "userSender") var userSender : String,
                       @SerializedName(value = "userReceiver") var userReceiver : String,
                       @SerializedName(value = "text") var text : String,
                       @SerializedName(value = "date") var date : Date) : RealmObject() {

                           constructor() : this("", "", "", Date())

}