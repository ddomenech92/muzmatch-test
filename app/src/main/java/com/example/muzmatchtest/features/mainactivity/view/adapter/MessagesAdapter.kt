package com.example.muzmatchtest.features.mainactivity.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.muzmatchtest.R
import com.example.muzmatchtest.features.mainactivity.model.ChatMessage
import com.example.muzmatchtest.features.mainactivity.model.MessageTypeOfView
import com.example.muzmatchtest.features.mainactivity.view.adapter.viewholders.MessagesViewHolder
import com.example.muzmatchtest.features.utils.MessageAdapterUtils

class MessagesAdapter(private val messageList: ArrayList<ChatMessage>, private var loggedUser: String) : RecyclerView.Adapter<MessagesViewHolder>() {

    fun setLoggedUser(loggedUser: String) {
        this.loggedUser = loggedUser
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        if(viewType == MessageTypeOfView.VIEW_MESSAGE_SEND_BY_ME.ordinal) {
            val itemView = layoutInflater.inflate(R.layout.view_holder_message_send_by_me, parent, false)
            return MessagesViewHolder(itemView, true)
        }
        else {
            val itemView = layoutInflater.inflate(R.layout.view_holder_message_send_by_other, parent, false)
            return MessagesViewHolder(itemView, false)
        }
    }

    override fun onBindViewHolder(holder: MessagesViewHolder, position: Int) {
        if(position > 0) {
            holder.setPreviousMessage(messageList[position-1])
        }
        else {
            holder.setPreviousMessage(null)
        }
        if(position < messageList.size-1) {
            holder.setNextMessage(messageList[position+1])
        }
        else {
            holder.setNextMessage(null)
        }
        holder.bindData(messageList[position])
    }

    override fun getItemViewType(position: Int): Int {
        return MessageAdapterUtils.getKindOfView(messageList[position], loggedUser)
    }

}