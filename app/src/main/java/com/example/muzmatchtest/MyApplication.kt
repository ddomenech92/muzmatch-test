package com.example.muzmatchtest

import android.app.Application
import com.example.muzmatchtest.dagger.DaggerMuzmatchComponent
import com.example.muzmatchtest.dagger.MuzmatchComponent
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApplication : Application() {

    lateinit var muzmatchComponent: MuzmatchComponent

    override fun onCreate() {
        super.onCreate()
        muzmatchComponent = DaggerMuzmatchComponent.builder().build()
        initRealm()
    }

    private fun initRealm() {
        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder().name("default.realm").build()
        Realm.setDefaultConfiguration(realmConfig)
    }

}