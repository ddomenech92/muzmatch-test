package com.example.muzmatchtest.realm

import io.realm.RealmModel
import io.realm.RealmResults

open class RealmDao {

    fun<T : RealmModel> RealmResults<T>.asLiveData() = RealmLiveData(this)

}