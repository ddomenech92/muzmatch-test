package com.example.muzmatchtest.realm

import androidx.lifecycle.LiveData
import com.example.muzmatchtest.features.mainactivity.model.ChatMessage
import io.realm.Realm
import io.realm.RealmResults

class MessagesDao(val realm: Realm) : RealmDao() {

    fun getMessages() : LiveData<RealmResults<ChatMessage>> {
        return realm.where(ChatMessage::class.java)
            .findAllAsync().asLiveData()
    }

    fun addMessage(chatMessage: ChatMessage) {
        realm.executeTransactionAsync {
            it.insert(chatMessage)
        }
    }

    fun clearMessages() {
        realm.executeTransactionAsync {
            val result = it.where(ChatMessage::class.java).findAll()
            result.deleteAllFromRealm()
        }
    }

}