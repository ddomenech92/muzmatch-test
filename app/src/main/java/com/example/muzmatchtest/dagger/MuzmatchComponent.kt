package com.example.muzmatchtest.dagger

import com.example.muzmatchtest.features.mainactivity.view.activity.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, MuzmatchViewModelModule::class])
interface MuzmatchComponent {

    fun inject(activity: MainActivity)

}