package com.example.muzmatchtest.dagger

import com.example.muzmatchtest.features.mainactivity.viewmodel.MessagesViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MuzmatchViewModelModule {

    @Provides
    @Singleton
    fun provideMessagesViewModel() : MessagesViewModel {
        return MessagesViewModel()
    }

}